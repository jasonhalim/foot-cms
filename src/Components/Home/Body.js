import React, { Fragment } from 'react'
import _ from 'lodash'
import LeftComponent from './Home Component/LeftComponent'
import axios from 'axios'

import GlobalContext from '../Context'

export default class Body extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      campaigns: []
    }
  }

  componentDidMount () {
    axios.get('http://localhost:3001/')
      .then(res => {
        const campaigns = res.data
        this.setState({ campaigns })
      })
  }

  render () {
    return (
      <GlobalContext.Consumer>
        {(context) => {
          return (
            <div className='flex center h-ex-full w-full'>
              <div className='Header w-ex-small main-tab-left main-tab-left-radius h-full border-right'>
                <LeftComponent triggerModal={this.triggerModal} context={context} />
              </div>
              <div className='w-full main-tab-right h-full'>right</div>

              {/* <nav>
            <Link to='/'>Home</Link>
            </nav> */}

            </div>
          )
        }}
      </GlobalContext.Consumer>
    )
  }
}

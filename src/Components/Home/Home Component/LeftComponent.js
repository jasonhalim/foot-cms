import React from 'react'
import _ from 'lodash'
import AddRounded from '@mui/icons-material/AddRounded'
import AddToDrive from '@mui/icons-material/AddToDrive'
import PostAdd from '@mui/icons-material/PostAdd'

export default class LeftComponent extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      dataOption: [
        { name: 'Microsite', icon: <AddToDrive /> },
        { name: 'Manage Items', icon: <PostAdd /> }
      ],
      active: 0
    }
  }

  // <img src="https://drive.google.com/uc?export=view&id=1fDDr6mpIpH7D7HgmL2Tj56Vh3cd_OUfl" alt="Flowers in Chania" width="460" height="345">

  render () {
    const { triggerModal } = this.props.context

    return (
      <div className='flex h-full w-full column align-item-c'>
        <div id='CreateNew' className='flex center m-top-half w-ex-full' style={{ color: 'black' }} onClick={() => triggerModal()}>
          <AddRounded fontSize='medium' />
          <p> Create New </p>
        </div>

        {
          this.state.dataOption.map(
            (data, idx) =>
              <div key={idx} className={`dummy flex w-full center ${idx === this.state.active ? 'active' : ''} m-top-${idx < 1 ? 'ex-small' : 'tiny'}`} onClick={() => this.setState({ active: idx })} id='MainOption'>
                <div className='w-ex-half flex end'>
                  {data.icon}
                </div>
                <div className='w-medium-ex-half flex start p-left-ex-tiny'>
                  {data.name}
                </div>
              </div>
          )
        }

      </div>
    )
  }
}

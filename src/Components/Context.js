import React, { createContext, Component } from 'react'

const { Provider, Consumer } = createContext({})

export class GlobalProvider extends Component {
  render () {
    return (
      <Provider value={this.props.value}>
        {this.props.children}
      </Provider>
    )
  }
}

export const WithContext = (Component) => {
  return (props) => (
    <Consumer>
      {contenx => <Component {...props} {...contenx} />}
    </Consumer>
  )
}

export default React.createContext({})
